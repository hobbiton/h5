package main

import (
	"bitbucket.org/hobbiton/h5/h5server"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
)

var (
	users            map[string]*h5server.TimeState = make(map[string]*h5server.TimeState)
	validPagePath                                   = regexp.MustCompile("^/([a-zA-Z0-9]+)$")
	validServicePath                                = regexp.MustCompile("^/s/([a-zA-Z0-9]+)/(start|stop|remaining)$")
	pagesDir         string
)

func pageHandler(w http.ResponseWriter, r *http.Request) {
	m := validPagePath.FindStringSubmatch(r.URL.Path)
	if m == nil {
		http.Error(w, "no index", http.StatusBadRequest)
		return
	}
	//ts := h5server.GetTimeState(m[1], users)
	h5server.GetTimeState(m[1], users)
	body, err := ioutil.ReadFile(filepath.Join(pagesDir, "index.html"))
	if err != nil {
		http.Error(w, "Data error", http.StatusNotFound)
	} else {
		w.Write(body)
	}
}

func serviceHandler(w http.ResponseWriter, r *http.Request) {
	m := validServicePath.FindStringSubmatch(r.URL.Path)
	if m == nil {
		http.Error(w, "no index", http.StatusBadRequest)
		return
	}
	ts := h5server.GetTimeState(m[1], users)
	switch m[2] {
	case "start":
		ts.Start()
	case "stop":
		ts.Stop()
		//case "remaining":
		//	ts.Remaining()
		//default:

	}
	js, err := json.Marshal(ts)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(js))
}

func main() {
	flag.StringVar(&pagesDir, "pagesDir", filepath.Join(filepath.Dir(os.Args[0]), "pages"), "Folder where the pages are to be found")
	flag.Parse()
	if _, err := os.Stat(pagesDir); err != nil {
		fmt.Printf("%s is not accessable", pagesDir)
		os.Exit(1)
	}
	http.HandleFunc("/", pageHandler)
	http.HandleFunc("/s/", serviceHandler)
	http.ListenAndServe(":8181", nil)
}
