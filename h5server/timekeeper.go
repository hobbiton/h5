package h5server

import (
	"fmt"
	"time"
)

type TimeState struct {
	name      string
	running   bool
	startTime time.Time
	left      time.Duration
}

func NewTimeState(name string, credit time.Duration) *TimeState {
	ts := new(TimeState)
	ts.name = name
	ts.running = false
	ts.left = credit
	return ts
}

func (state *TimeState) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("{\"name\":\"%s\", \"running\":%t, \"time_left\":\"%s\"}", state.name, state.running, state.Remaining())), nil
}

func (state *TimeState) Start() {
	if !state.running {
		state.startTime = time.Now()
		state.running = true
	}
}

func (state *TimeState) Stop() {
	if state.running {
		state.left -= time.Since(state.startTime)
		state.running = false
	}
}

func (state *TimeState) Remaining() string {
	var n time.Duration
	if state.running {
		n = state.left - time.Since(state.startTime)
	} else {
		n = state.left
	}
	h := n / time.Hour
	n -= h * time.Hour
	m := n / time.Minute
	n -= m * time.Minute
	s := n / time.Second
	return fmt.Sprintf("%02d:%02d:%02d", h, m, s)
}

func (state *TimeState) String() string {
	return fmt.Sprintf("Name = %s : Running = %t : Start = %s : Left = %d\n", state.name, state.running, state.startTime.Format(time.RFC3339), state.left)
}

func GetTimeState(name string, users map[string]*TimeState) *TimeState {
	var ts *TimeState
	var isAvailable bool
	ts, isAvailable = users[name]
	if !isAvailable {
		ts = NewTimeState(name, time.Hour)
		users[name] = ts
	}
	return ts
}
