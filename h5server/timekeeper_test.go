package h5server

import (
	"crypto/md5"
	"fmt"
	"testing"
	"time"
)

const name = "johndoe"
const otherName = "janedoe"

func TestStart(t *testing.T) {
	timeState := NewTimeState(name, time.Minute)
	fmt.Printf(timeState.String())
	assertNotRunning(timeState, t)
	if time.Minute != timeState.left {
		t.Errorf("expected '%s', was '%s'", time.Minute, timeState.left)
	}
	remainingStr1 := timeState.Remaining()
	timeState.Start()
	fmt.Printf(timeState.String())
	assertRunning(timeState, t)
	remainingStr2 := timeState.Remaining()
	fmt.Printf("expecting '%s' > '%s'\n", remainingStr1, remainingStr2)
	if remainingStr1 == remainingStr2 {
		t.Errorf("expected '%s' > '%s'", remainingStr1, remainingStr2)
	}
	// calling start again should be a no-op
	startTime1 := timeState.startTime
	timeState.Start()
	if startTime1 != timeState.startTime {
		t.Errorf("expecting no change")
	}
	timeState.Stop()
	fmt.Printf(timeState.String())
	assertNotRunning(timeState, t)
	start := timeState.startTime
	left := timeState.left
	fmt.Printf("SAVED : Start = %s : Used = %d\n", start.Format(time.RFC3339), left)
	timeState.Start()
	fmt.Printf(timeState.String())
	if start == timeState.startTime {
		t.Fatalf("Start should be reset")
	}
	if left != timeState.left {
		t.Fatalf("Left should have stayed the same")
	}
	assertRunning(timeState, t)
	timeState.Stop()
	fmt.Printf(timeState.String())
	assertNotRunning(timeState, t)
	fmt.Printf("SAVED : Start = %s : Used = %d\n", start.Format(time.RFC3339), left)
	if left <= timeState.left {
		t.Fatalf("Used should have decreased %d <= %d ", left, timeState.left)
	}
}

func TestStop(t *testing.T) {
	timeState := NewTimeState(name, time.Minute)
	fmt.Printf(timeState.String())
	assertNotRunning(timeState, t)
	timeState.Stop()
	fmt.Printf(timeState.String())
	assertNotRunning(timeState, t)
}

func TestRemaining(t *testing.T) {
	tsAll := NewTimeState(name, time.Hour*12+time.Minute*23+time.Second*34)
	fmt.Printf("Remaining = %s\n", tsAll.left.String())
	assertEquals("12:23:34", tsAll.Remaining(), t)
	tsHour := NewTimeState(name, time.Hour)
	fmt.Printf("Remaining = %s\n", tsHour.left.String())
	assertEquals("01:00:00", tsHour.Remaining(), t)
	tsMin := NewTimeState(name, time.Hour-time.Minute)
	fmt.Printf("Remaining = %s\n", tsMin.left.String())
	assertEquals("00:59:00", tsMin.Remaining(), t)
	tsSec := NewTimeState(name, time.Minute-time.Second)
	fmt.Printf("Remaining = %s\n", tsSec.left.String())
	assertEquals("00:00:59", tsSec.Remaining(), t)
}

func TestGetTimeState(t *testing.T) {
	var users map[string]*TimeState = make(map[string]*TimeState)
	ts1 := GetTimeState(name, users)
	if ts1 == nil {
		t.Fatalf("initialized TimeState expected")
	}
	ts2 := GetTimeState(name, users)
	if ts2 == nil {
		t.Fatalf("initialized TimeState expected")
	}
	if ts2 != ts1 {
		t.Fatalf("same TimeState expected")
	}
	ts3 := GetTimeState(otherName, users)
	if ts3 == nil {
		t.Fatalf("initialized TimeState expected")
	}
	if ts3 == ts1 {
		t.Fatalf("different TimeState expected")
	}
}

func assertEquals(expected string, s string, t *testing.T) {
	if expected != s {
		t.Errorf("expected '%s', was '%s'", expected, s)
	}
}

func assertRunning(state *TimeState, t *testing.T) {
	if !state.running {
		t.Fatalf("Should be running")
	}
}

func assertNotRunning(state *TimeState, t *testing.T) {
	if state.running {
		t.Fatalf("Should not be running")
	}
}

func TestMD5(t *testing.T) {
	data := []byte("abc")
	md5Val := fmt.Sprintf("%x", md5.Sum(data))
	fmt.Printf("MD5 = %s\n", md5Val)
	assertEquals("900150983cd24fb0d6963f7d28e17f72", md5Val, t)

	data = []byte("g3BZutwZL7")
	md5Val = fmt.Sprintf("%x", md5.Sum(data))
	fmt.Printf("MD5 = %s\n", md5Val)
	assertEquals("3d82344cf32c7dad84b3076868713524", md5Val, t)
}
